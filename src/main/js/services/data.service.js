(function () {
  'use strict';

  angular.module('coderuseApp.services')
          .factory('dataService', ['$http', '$q', '$timeout', 'serviceActionMap',
            function ($http, $q, $timeout, serviceActionMap) {

              var queue = [],
                      execNext = function () {
                        var task = queue[0];
                        $http(task.c).then(function (res) {
                          queue.shift();

                          task.d.resolve(res.data);

                          if (queue.length > 0) {
                            execNext();
                          }
                        }, function (err) {
                          queue.shift();

                          task.d.reject(err);

                          if (queue.length > 0) {
                            execNext();
                          }
                        });
                      };

              var dataService = {
                getData: function (actionName) {
                  var url = serviceActionMap[actionName] ?
                          serviceActionMap[actionName].url : null;
                  var d = $q.defer();
                  if (url === null) {
                    var msg = 'Action: "' + actionName + '" is not defined';
                    $timeout(function () {
                      d.reject(msg);
                    });
                    return d;
                  }
                  else {
                    queue.push({
                      c: {
                        method: 'GET',
                        url: url
                      },
                      d: d
                    });
                    if (queue.length === 1) {
                      execNext();
                    }
                    return d.promise;
                  }
                }
              };

              return dataService;
            }]);
})();