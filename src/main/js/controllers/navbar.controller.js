(function () {
  'use strict';

  angular.module('coderuseApp.controllers')
          .controller('navbarCtrl', ['$scope', '$state', 'store',
            function ($scope, $state, store) {
              angular.extend($scope, {
                logout: function () {
                  store.deleteStore('loggedInUser');
                  $state.go('login');
                }
              });
            }]);
})();