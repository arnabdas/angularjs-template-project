module.exports = function (grunt) {
  'use strict';

  var pkg = grunt.file.readJSON('package.json');
  for (var taskName in pkg.devDependencies) {
    if (taskName.indexOf('grunt-') > -1) {
      grunt.loadNpmTasks(taskName);
    }
  }

  var config = require('config');
  var dependencyFiles = config.get('dependencyFiles'),
          bowerDependencyFiles = config.get('bowerDependencyMinFiles');

  grunt.initConfig({
    pkg: pkg,
    connect: {
      options: {
        hostname: '0.0.0.0'
      },
      build: {
        options: {
          port: config.get('portConfig.build'),
          base: 'dist',
          keepalive: true,
          open: {
            target: 'http://localhost:<%= connect.build.options.port %>/index.html'
          }
        }
      },
      dev: {
        options: {
          port: config.get('portConfig.dev'),
          base: 'src/main',
          open: {
            target: 'http://localhost:8999/index.html'
          }
        }
      }
    },
    wiredep: {
      task: {
        src: 'src/main/index.html'
      }
    },
    injector: {
      options: {
        relative: true,
        addRootSlash: false
      },
      devJSDependencies: {
        files: {
          'src/main/index.html': dependencyFiles.js
        }
      },
      devCSSDependencies: {
        files: {
          'src/main/index.html': dependencyFiles.css
        }
      },
      prodDependencies: {
        files: {
          'dist/index.html': ['dist/app.min.css', 'dist/app.min.js']
        }
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        ignores: ['**/build/**/*', '**/vendor/**/*', '**/node_modules/**/*']
      },
      src: ['src/main/**/*.js']
    },
    less: {
      development: {
        files: {
          // target.css file: source.less file
          'src/main/css/app.css': 'src/main/assets/less/app.less'
        }
      },
      production: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          'build/_custom.css': 'src/main/assets/less/app.less'
        }
      }
    },
    watch: {
      options: {
        spawn: false
      },
      less: {
        files: [
          'src/main/assets/less/**/*.less'
        ],
        tasks: ['less:development']
      },
      js: {
        files: [
          'src/main/assets/js/**/*'
        ],
        tasks: ['jshint']
      }
    },
    html2js: {
      options: {
        quoteChar: '\'',
        base: 'src/main',
        singleModule: true,
        module: 'coderuseApp',
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        }
      },
      main: {
        src: ['src/main/templates/**/*.html'],
        dest: 'build/_templates.js'
      }
    },
    concat: {
      options: {
      },
      custom: {
        src: dependencyFiles.js.concat(['build/_templates.js']),
        dest: 'build/_custom.js'
      },
      allJS: {
        src: bowerDependencyFiles.js.concat(['build/_custom.js']),
        dest: 'dist/app.min.js'
      },
      allCSS: {
        src: bowerDependencyFiles.css.concat(['build/_custom.css']),
        dest: 'dist/app.min.css'
      }
    },
    bower_concat: {
      all: {
        dest: 'build/_bower.js',
        cssDest: 'build/_bower.css'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      all: {
        files: {
          'build/_custom.min.js': ['build/_custom.js']
        }
      }
    },
    clean: ['build', 'dist'],
    copy: {
      main: {
        files: [
          // includes files within path
          {src: 'src/main/index-build.html', dest: 'dist/index.html'},
          // makes all src relative to cwd
          {expand: true, cwd: 'src/main', src: ['i18n/**', 'data/**'], dest: 'dist/'}
        ]
      }
    }
  });

  // Development
  grunt.registerTask('default', ['wiredep', 'less:development', 'injector:devJSDependencies', 'injector:devCSSDependencies', 'connect:dev', 'watch']);

  grunt.registerTask('build', [
    'clean', 'bower_concat', 'less:production',
    'html2js', 'concat:custom', 'concat:allJS',
    'concat:allCSS', 'copy', 'injector:prodDependencies', 'connect:build'
  ]);
};
